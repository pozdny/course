﻿Курс по PHP. #1

Требуется скачать и установить
	VirtualBox	https://www.virtualbox.org/wiki/Downloads
	Vagrant		https://www.vagrantup.com/downloads.html
	SourceTree	http://www.sourcetreeapp.com
	Putty		http://the.earth.li/~sgtatham/putty/latest/x86/putty-0.63-installer.exe


Настройка Vagrant
	Запускаем cmd

	chcp 1251
	vagrant box add Course https://oss-binaries.phusionpassenger.com/vagrant/boxes/latest/ubuntu-14.04-amd64-vbox.box
	vagrant init Course
	vagrant up

