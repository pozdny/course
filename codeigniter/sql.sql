-- create table test(
--     id INT NOT NULL AUTO_INCREMENT,
--     user_id INT NOT NULL,
--     text VARCHAR(50),
--     PRIMARY KEY (id)
-- );
-- alter table test add column `time` INT DEFAULT 0 AFTER id;
-- desc test

-- insert into comments (message_id, user_id, created) values (1,1,123);

-- explain select * from test where user_id = 20;
--create index `for_search` on test (user_id);


-- explain select * from test where user_id = 20;

select * from test;
explain select * from test where user_id = 2 AND text = 'abc';