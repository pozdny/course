<?php
class Messages extends MY_Controller {
	 public function __construct() {
            parent::__construct();
            $this->load->model('Messages_model');
        }

        public function index() { 
            if ($_POST) {               
                $user_id = $_POST['user_id'];
                $text = $_POST['text'];
                
                $error = Messages_model::make($user_id, $text);
            }
            
            $obj = new Messages_model();
            $list = $obj->getList();
            
            $this->load->view('messages', array(
                'list' => $list,
                'error' => isset($error) ? $error : null
            ));
        }
        
        public function remove() {           
            $id = isset($_GET['id']) ? $_GET['id'] : null;
            Messages_model::remove($id);
            echo 'success';
            die();
        }
        public function edit() {
            $id = isset($_GET['id']) ? $_GET['id'] : null;
            $obj = Messages_model::getForId($id);            
            if ($_POST && isset($_POST['text'])) {                            
                $id = $_POST['id'];
                $obj = Messages_model::getForId($id);   
                $obj->text = $_POST['text'];                 
                Messages_Model::save($obj);
                echo 'success';
                die();
            }           
            $this->render('message_edit', array(
                'message' => $obj
            ));
        }
        
}

