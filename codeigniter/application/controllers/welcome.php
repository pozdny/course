<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {
	 public function __construct() {
            parent::__construct();
            if (!$this->user) {
                $this->redirect('/index.php');
            }
            $this->load->model('Messages_model');

        }
        public function index() {  
            $err = false;
            if ($_POST) {
                $user_id = $_POST['user_id'];
                $text = $_POST['text'];               
                $err = Messages_model::make($user_id, $text);
                if (!$err['error']) {
                    echo 'success';
                    die();
                }
                else{
                    alert($err['msg'][0]);
                }
            }
            $obj = new Messages_model();
            $list = $obj->getListOneUser($this->user->id);
            $obj = new Users_Model();
            $users = $obj->getList(); 
            $this->render('messages', array(
                'list' => $list,
                'users' => $users,
                'err' => $err
            ));
        }     
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */