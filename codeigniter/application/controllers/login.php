<?php
class Login extends MY_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->model('Users_model');
    }

    public function index() {
        if($this->user){
            $this->redirect('/welcome');
        }
        $err = false;
            if ($_POST) {
                $login = $_POST['login'];
                $password = $_POST['password'];
                // todo: временно для локального тестирования
                sleep(1);
                $user = Users_Model::getForLoginPassword($login, $password);
                if (!$user) {
                    echo 'not found';
                    die();
                } else {
                    setcookie('user_id', $user->id, 0, '/');
                    echo 'success';
                    die();
                }
            }
            
            $this->render('login', array(
                'err' => $err
            ));
            
    }
    public function quit() {
            setcookie('user_id', 0, 0, '/');
            $this->redirect('/login');
        }

}