<?php
class Registration extends MY_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->model('Users_model');
    }

    public function index() {
        if($this->user){
            $this->redirect('/welcome');
        }
        $err = false;
        if ($_POST) {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $fname = $_POST['first_name'];
            $lname = $_POST['last_name'];
            $err = Users_model::make($login, $password, $fname, $lname);
            if (!$err['error']) { 
                $id = $err['res'];
                setcookie('user_id', $id, 0, '/');               
                echo 'success';
                die();
            }
            else{                
                echo $err['msg'][0];
                die();
            }
        }

        $this->render('registration', array(
            'err' => $err
        ));
    }
    

}