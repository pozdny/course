<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link href="/public/dist/bootstrap.min.css" rel="stylesheet">                
        <script src="/public/jquery-2.1.1.js"></script>
    </head>
    <body>
        <div class="container">            
            <p id="text">hello</p>
            <button id="clickMe" class="btn btn-default">Нажми</button>
        </div>
    </body>
    <script>
        $('#clickMe').click(function() {
            $.get(
                '/index.php/test/ajax',
                {
                    title: 'Hello',
                    body: '<p>Reload for repeat</p>'
                },
                function(data) {
                    $('body').html(data);
                }
            );
        });
        
//        var obj = {
//            counter: 0,
//            query: function(callback) {
//                setTimeout(function() {
//                    callback([1,2,3]);
//                }, 1000);
//                
//                return true;
//            }
//        }
//        
//        var list = [],
//            res = obj.query(function(data) {
//                list = data;
//                console.log(list);
//            });
//            
//        console.log(res);
    </script>
</html>

