<h3>Авторизация</h3>
<?php if ($err): ?>
    <p style="color:red"><?php echo $err ?>
    <?php endif ?>
<div>
    <div class="row">
        <div class="form-group col-xs-4">
            <label for="login">Логин</label>
            <input id="login" type="text" placeholder="Login" name="login" class="form-control">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-4">     
        <label for="password">Пароль</label>
        <input id="password" type="password" placeholder="Password" name="password" class="form-control">
    </div>
    </div>
    <span id="btn_login" class="btn btn-primary">Войти</span>
    <br><br>
    <span id="label" class="label label-default" style="display:none"></span>
</div>


    
<p><a href="/registration">Регистрация</a></p>
