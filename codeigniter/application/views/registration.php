<h3>Регистрация</h3>
<?php if ($err && $err['error']): ?>
    <ul style="color:red">
        <?php foreach ($err['msg'] as $value): ?>
            <li><?php echo $value; ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
<form id="formReg">   
    <div class="row">
        <div class="form-group col-xs-4">
            <label for="login">Логин*</label>
            <input type="text" placeholder="Login" name="login" class="form-control" id="login">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-4">
            <label for="password">Пароль*</label>
            <input type="password" placeholder="Password" name="password" class="form-control" id="password">   
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-4">
            <label for="first_name">Имя*</label>
            <input type="text" placeholder="Имя" name="first_name" class="form-control" id="first_name">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-4">
            <label for="last_name">Имя*</label>
            <input type="text" placeholder="Фамилия" name="last_name" class="form-control" id="last_name">
        </div>
    </div>
    <br><br>
    <button type="button" id="btn_reg" class="btn btn-primary" data-loading-text="Подождите...">Зарегистрироваться</button>
    <a href="/login">Отмена</a>
</form>
