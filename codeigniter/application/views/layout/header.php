<!DOCTYPE html>
<html>
    <head>
        <link href="/public/dist/bootstrap.min.css" rel="stylesheet">
        <link href="/public/main.css" rel="stylesheet">
        <script src="/public/jquery-2.1.1.js"></script>
        <script src="/public/main.js"></script>
        <title>Page hello</title>
        <script>
            <?php if (!$user): ?>
                $(function() {
                    formLogin.init();
                    formRegistration.init();                   
                });
            <?php else: ?>
                $(function(){
                    delMessages.init();
                    addMessages.init();
                    changeMessages.init();
                });
            <?php endif ?>
        </script>
    </head>
    <body>
        <div class="container">
            <div style="margin-bottom: 20px; border-bottom: 1px solid silver">
                <?php if ($user): ?>
                    Логин: <?php echo $user->login?> 
                    <a href="/index.php/login/quit">выход</a>
                <?php else: ?>
                    Пользователь не авторизован
                <?php endif ?>
            </div>
