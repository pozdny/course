<h3>Редактировать сообщение</h3>
<form id="formChangeMsg">
    <div class="row">
        <div class="form-group col-xs-4">
            <label for="text">Сообщение</label>
            <textarea type="text" name="text" rows="5" id="text" class="form-control"><?php echo $message->text; ?></textarea>   
        </div>
    </div>           
    <input type="hidden" name="id" value="<?php echo $message->id; ?>" >    
    <button type="button" id="btn_change" data-loading-text="Подождите..." class="btn btn-primary" autocomplete="off">
        Сохранить
    </button>
    <a href="/welcome">Отмена</a>
           
</form>