<div class="row">
    <div class="col-xs-6">
        <table class="table table-bordered">
    <tr>
        <th>ID</th>
        <th>User_id</th>
        <th>Сообщение</th>
        <th>Удалить</th>
        <th>Редактировать</th>
    </tr>
    <?php foreach ($list as $messages): ?>
        <tr>
            <td><?php echo $messages->id ?></td>
            <td><?php echo $messages->user_id ?></td>
            <td><?php echo $messages->text ?></td>
            <td>
                <a href="#" class="a_del text-primary" data-id="<?php echo $messages->id ?>">
                    Удалить
                </a>
            </td>
            <td>
                <a href="/messages/edit?id=<?php echo $messages->id ?>">
                    Редактировать
                </a>
            </td>
        </tr>
    <?php endforeach ?>
</table>
    </div>
</div>

<h4>Добавление сообщения</h4>
<?php if ($err && $err['error']): ?>
    <ul style="color:red">
        <?php foreach ($err['msg'] as $value): ?>
            <li><?php echo $value; ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>

<form id="formAddMsg">
    <div class="row">
        <div class="form-group col-xs-4">
            <label for="user_id">Имя*</label>
            <select name="user_id" id="user_id" class="form-control">
                <?php foreach ($users as $user): ?>
                <option value="<?php echo $user->id?>">
                    <?php echo $user->first_name.' '.$user->last_name?>
                </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-4">
            <label for="text">Сообщение</label>
            <textarea type="text" name="text" rows="5" id="text" class="form-control"></textarea>
        </div>
    </div>
    <button type="button" id="btn_msg" data-loading-text="Подождите..." class="btn btn-primary" autocomplete="off">
        Добавить
    </button>
</form>


