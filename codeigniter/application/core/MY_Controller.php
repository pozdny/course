<?php
//Базовый контроллер от которого будем наследовать наши контроллеры
abstract class MY_Controller extends CI_Controller{
     protected $user; 
    //В конструкторе задаем кодировку для страниц
    public function __construct() {
        parent::__construct();
        // подключаем модель
            $this->load->model('Users_Model');
            // получаем данные о пользвателе если он авторизован
            $this->authUser();
            
            header('Content-Type: text/html; charset=utf-8');
    }
    public function render($template, $data = array()){
        $this->load->view('layout/header', array(
            'user' => $this->user
        ));
        $this->load->view($template, $data);
        $this->load->view('layout/footer');
    }

    public function redirect($location) {
        header("location: $location");
        die();
    }

    private function authUser() {
        // проверяем, что есть cookie с нужным ключом
        if (isset($_COOKIE['user_id'])) {
            // проверяем, что сущетсвует пользователь с таким id
            $user = Users_Model::getForId($_COOKIE['user_id']);
            if ($user) {
                $this->user = $user;                
            }
        }
    }        
}
