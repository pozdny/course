<?php
class Users_model extends CI_Model{
    public $login;
    public $password;
    public $first_name;
    public $last_name;
    
    public function getList() {
        $query = $this->db->get('users');
        return $query->result();
    }
    public static function save($user) {
        $obj = new self;
        $res = $obj->db->update('users', $user, array('id' => $user->id));
    }

    public static function remove($id) {
        if (!$id) {
            return false;
        }
        
        $obj = new self;
        $obj->db->delete('users', array('id'=>$id));
    }
    
    public static function make($login, $password, $firstName, $lastName) {
        $msg = array();
        if (!$firstName || !$lastName || !$login || !$password) {
            $msg[] = 'Введите все данные';
            return array(
                'error' => 1,
                'msg' => $msg
            );
        }
        if (Users_model::isExists('login', $login)) {         
            $msg[] = 'Такой логин уже существует';
        }
        if(sizeof($msg)){
            return array(
                'error' => 1,
                'msg' => $msg
            );
        }
        
        $obj = new self;
        $obj->login = $login;
        $obj->password = md5($password);
        $obj->first_name = $firstName;
        $obj->last_name = $lastName;        
        $obj->db->insert('users', $obj); 
        $last_id = $obj->db->insert_id();
        return array(
            'error'=>0,
            'res'=>$last_id
            );
    }
    
    public static function isExists($str, $param) {
        $obj = new self;
        $query = $obj->db->get_where('users', array($str=>$param));
        $res = $query->result();
        return (bool)$res;
    }
    /**
     * Получить объект пользователя для пары login и пароль
     * @param string $login
     * @param string $password
     * @return false|stdClass
     */
    public static function getForLoginPassword($login, $password) {
        $obj = new self;

        $query = $obj->db->get_where('users', array(
            'login' => $login,
            'password' => md5($password)
        ));
        $res = $query->result();
        
        return $res ? $res[0] : false;
    }
    /**
     * 
     * @param type $id
     * @return boolean
     */
    public static function getForId($id) {
        $obj = new self;
        $query = $obj->db->get_where('users', array('id' => $id));
        if ($res = $query->result()) { 
            return $res[0];
        } else {
            return false;
        }
    }
}