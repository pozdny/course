<?php
class Messages_model extends CI_Model{
    public $user_id;
    public $text;
    
    public function getList() {
        $query = $this->db->get('messages');
        return $query->result();
    }
    public function getListOneUser($id) {
        $query = $this->db->get_where('messages', array('user_id'=>$id));
        return $query->result();
    }
    public static function save($message) {
        $obj = new self;
        $res = $obj->db->update('messages', $message, array('id' => $message->id));
        
    }
    public static function remove($id) {
        if (!$id) {
            return false;
        }        
        $obj = new self;
        $obj->db->delete('messages', array('id'=>$id));
    }
    public static function isExists($str, $param) {
        $obj = new self;
        $query = $obj->db->get_where('messages', array($str=>$param));
        $res = $query->result();
        return (bool)$res;
    }
    public static function make($user_id, $text) {
        $msg = array();
        if (!$user_id || !$text) {
            $msg[] = 'Введите все данные';
            return array(
                'error' => 1,
                'msg' => $msg
            );
        }   
          
        if (!Users_model::isExists('id', $user_id)) {
            $msg[] = 'Пользователя не сущестует';
            
        }  
        if(sizeof($msg)){
            return array(
                'error' => 1,
                'msg' => $msg
            );
        }
        $obj = new self;
        $obj->user_id = $user_id; 
        $obj->text = $text;
        
        $res = $obj->db->insert('messages', $obj);
        
        return array('error'=>0);
    }
    public static function getForId($id) {
        $obj = new self;
        $query = $obj->db->get_where('messages', array('id' => $id));
        if ($res = $query->result()) { 
            return $res[0];
        } else {
            return false;
        }
    }
}

