var formLogin = {
    onLoad:false,
    s: {
        btnLogin: '#btn_login',
        inputPassword: '#password',
        inputLogin: '#login',
        label:'#label'
    },
    
    init: function() {
        $(this.s.btnLogin).click(this.login.bind(this));
    },
    
    login: function() {
        var login = $(this.s.inputLogin).val(),
            password = $(this.s.inputPassword).val();
        if(this.onLoad){
            return;
        }        
        this.onLoad = true;
        $(this.s.btnLogin).toggle();
        $(this.s.label).text('Идет загрузка').show();
        
        $.post(
            '/index.php/login',
            {
                login: login,
                password: password
            },
            this.afterLogin.bind(this)
        );
    },
    
    afterLogin: function(data) {
        this.onLoad = false;
        if (data === 'not found') {
            $(this.s.label).toggleClass('label-warning')
                           .toggleClass('label-default')
                           .text('Неверный логин или пароль');
            return this.resetForm();
        } else if (data === 'success') {
            window.location = '/index.php/welcome/';
        } else {
            $(this.s.label).toggleClass('label-warning')
                           .toggleClass('label-default')
                           .text('Неверный ответ сервера');
            return this.resetForm();
        }                    
    },
    resetForm: function() {
        setTimeout(function() {
            $(this.s.label)
                    .toggle()
                    .toggleClass('label-default')
                    .toggleClass('label-warning');
            $(this.s.btnLogin).toggle();
        }.bind(this), 2000);
    }
};
var formRegistration = {
    s: {
        btn: '#btn_reg',
        form: '#formReg',
    },
    init: function() {
        $(this.s.btn).click($.proxy(this, "registration"));
    },
    registration:function(){ 
        var arr = $(this.s.form).serialize();
        var btn = $(this.s.btn);
        btn.addClass('disabled').attr({disabled:true});
        btn.button('loading');
        $.ajaxSetup({
            url:"/registration",
            type: "post",
            dataType:"html",
            cache:false,
            success: function(data){
                if(data){
                    if(data == 'success'){
                        location.href = '/welcome';
                    }
                    else{
                        alert(data);
                        btn.removeClass('disabled').attr({disabled:false});
                        btn.button('reset');
                    }
                }
            }
        });
        $.ajax({
            data:arr
        });
    }
};
var delMessages = { 
    s: {
        del: '.a_del'
    },
    init:function(){  
        var del = $(this.s.del); 
        $.each(del, function(){
            $(this).click(function(e){
                var id = $(this).data('id');
                $(this).unbind("click").text('Удаляется...').removeClass('text-primary').addClass('text-danger');
                $.ajaxSetup({
                    url:"/messages/remove",
                    type: "get",
                    dataType:"html",
                    cache:false,
                    success: function(data){
                        if(data){
                            if(data === 'success'){
                                location.href = '/welcome';
                            }
                            else{
                                alert('Ошибка при удалении');                                
                            }
                        }
                    }
                });
                $.ajax({
                    data:{
                        id:id
                    }
                });
            });
        });
        
    }
        
};
var addMessages = { 
    s: {
        btn: '#btn_msg',
        form: '#formAddMsg',
    },
    init: function() {
        $(this.s.btn).click($.proxy(this, "add"));
    },
    add:function(){
        var arr = $(this.s.form).serialize();
        var btn = $(this.s.btn);
        btn.addClass('disabled').attr({disabled:true});
        btn.button('loading');
        $.ajaxSetup({
            url:"/welcome",
            type: "post",
            dataType:"html",
            cache:false,
            success: function(data){
                if(data){
                    if(data == 'success'){
                        location.href = '/welcome';
                    }
                    else{
                        alert(data);    
                        btn.removeClass('disabled').attr({disabled:false});
                        btn.button('reset');
                    }
                }
            }
        });
        $.ajax({
            data:arr
        });
    }
};
var changeMessages = { 
    s: {
        btn: '#btn_change',
        form: '#formChangeMsg',
    },
    init: function() {
        $(this.s.btn).click($.proxy(this, "change"));
    },
    change:function(){
        var arr = $(this.s.form).serialize();
        var btn = $(this.s.btn);
        btn.addClass('disabled').attr({disabled:true});
        btn.button('loading');
        $.ajaxSetup({
            url:"/messages/edit",
            type: "post",
            dataType:"html",
            cache:false,
            success: function(data){
                if(data){
                    if(data == 'success'){
                        location.href = '/welcome';
                    }
                    else{
                        alert(data);    
                        btn.removeClass('disabled').attr({disabled:false});
                        btn.button('reset');
                    }
                }
            }
        });
        $.ajax({
            data:arr
        });
    }
};


