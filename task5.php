<?php
require_once ('class/MobileOperator.php');
require_once ('class/Phone.php');

$filename = 'numbers.txt'; 
$numbers = array();
if(file_exists($filename)){
    $lines = file($filename);    
    foreach ($lines as $value){
        $numbers[] = trim($value);
    } 
}
else{ 
    //создаем массив с номерами телефонов
    $numbers = array(
        '89139115625',
        '89139492786',
        '89139135645'
    );
    $str = '';
    foreach ($numbers as $value){
        $str.=$value.PHP_EOL;
    }
    file_put_contents($filename, $str);
}


//исходные данные
$rightPhone = '89139135645'; //есть в списке
$rightPhone1 = '89139115625';//есть в списке
$wrongPhone = '89139492711'; //нет в списке
//
//создаем объект $megafon класса MobileOperator
$megafon = new MobileOperator();

//на каждый номер в массиве $numbers создаем объект класса Phone
foreach ($numbers as $number){ 
    $megafon->addPhone($number);
}

//возвращаем объект класса Phone если найдено соответствие с номером в списке
$myPhone = $megafon->findByNumber($rightPhone);

//вызываем номер который есть в списке
$myPhone->call($rightPhone1); // поступил входящий звонок с номера $rightPhone1

//вызываем номер которого нет в списке
$myPhone->call($wrongPhone); // телефон $wrongPhone не найден



        
