<?php
$filename = 'files/numbers.txt'; 
$numbers = array();
if(file_exists($filename)){
    $lines = file($filename);    
    foreach ($lines as $value){
        $numbers[] = trim($value);
    } 
}
else{ 
    //создаем массив с номерами телефонов
    $numbers = array(
        '89139115625',
        '89139493333',
        '89139134444',
        '89139875555'
    );
    $str = '';
    foreach ($numbers as $value){
        $str.=$value.PHP_EOL;
    }
    file_put_contents($filename, $str);
}
