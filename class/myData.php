<?php
class myData {
    public $name;
    public $last_name;
    protected $age;
    
    public function __construct($name, $last_name) {
        $this->name = $name;
        $this->last_name = $last_name;
    }
    
    public function setAge($age){
        $this->age = $age;
    }
    public function getAge(){
        return $this->age;
    }
}
