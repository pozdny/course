<?php
class Phone {
    protected $mobileOperator;
    protected $number;
    
    public function __construct($mobileOperator, $number) {
        $this->mobileOperator = $mobileOperator;
        $this->setNumber($number);
    }

    public function call($number) {
        $phone = $this->mobileOperator->findByNumber($number);       
        if($phone){ 
            $res = $this->accept($phone);
        }
        else{
            $res = array(
                'result' => false,
                'msg' => 'Телефон '.$number.' не найден'
            );
                            
        }
        return $res;
    }
    public function accept($phone) {
        $res = array(
            'result' => true,
            'msg' => 'Поступил входящий звонок с номера '.$phone->getNumber()
        );
        return $res;          
    }
    public function getNumber() {
        return $this->number;
    }
    public function setNumber($number){
        $this->number = $number;
    }
}
