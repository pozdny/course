<?php
class MobileOperator {
    protected $phones = array();
    
    public function addPhone($number){
        $this->phones[] = new Phone($this, $number);
    }
    public function findByNumber($number) {        
        foreach ($this->phones as $phone){            
            if($phone->getNumber() == $number){
                return $phone;
            }
        }
        return false;
    }
}
