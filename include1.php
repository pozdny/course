<?php
function myData($name, $lastname, $year){
    $arr = array(
        'name' => $name,
        'lastname' => $lastname,
        'year' => $year
    );
    return $arr;
}

function changeArray(&$arr, $myData){
    $arr['name'] = $myData['name'];
    $arr['lastname'] = $myData['lastname'];
    $arr['year'] = $myData['year'];
}
//инициализация
function init($name, $lastname, $year){
    $obj = (object)array(
        'name' => $name,
        'lastname' => $lastname,
        'year' => $year
    );
    return $obj;
}
//изменение свойств
function setData($obj, $name, $lastname, $year){
    $obj->name = $name;
    $obj->lastname = $lastname;
    $obj->year = $year;
}


