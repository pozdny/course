<?php
header('Content-Type: text/html; charset=utf-8');
require_once ('class/MobileOperator.php');
require_once ('class/Phone.php');
require_once ('include/numbers.php');

//исходные данные
$res = array();
$rightPhone  = '89139493333'; //есть в списке
$rightPhone1 = '89139115625'; //есть в списке
$wrongPhone  = '89139492711'; //нет в списке
$wrongPhone1 = '89139492712'; //нет в списке
$i = 1;
//создаем объект $megafon класса MobileOperator
$megafon = new MobileOperator();

//на каждый номер в массиве $numbers создаем объект класса Phone
foreach ($numbers as $number){ 
    $megafon->addPhone($number);
}
//возвращаем объект класса Phone если найдено соответствие с номером в списке
$myPhone = $megafon->findByNumber($rightPhone);

//вызываем номер который есть в списке
$res[] = $myPhone->call($rightPhone1);  

//вызываем номер которого нет в списке
$res[] = $myPhone->call($wrongPhone); 

//вызываем номер которого нет в списке
$res[] = $myPhone->call($wrongPhone1); 

//выводим таблицу с результатом
include "templates/view.php";
